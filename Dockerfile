FROM ruby
MAINTAINER Akash Srivastava <akashsrvstv@yahoo.in>
ADD app home/app
ADD test home/test
ADD Gemfile home/Gemfile
ADD Gemfile.lock home/Gemfile.lock
ENV ROOT_DIR=/home
RUN cd home && gem install -q --no-rdoc --no-ri bundler && bundle install
WORKDIR home/app
EXPOSE 9292