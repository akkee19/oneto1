require 'test/unit'
require 'rack/test'
require_relative '../app/rack_app.rb'
class RackAppTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    @app = RackApp.app.to_app
  end

  def testing_with_correct_url
    get '/one', format: 'json'
    assert_equal last_response.status, 200
    json_response = JSON.parse(last_response.body)
    assert_equal json_response['key'], 'one'
    assert_equal json_response['value'], 1
  end

  def testing_with_incorrect_urls
    get '/', format: 'json'
    assert_equal last_response.status, 404
    json_response = JSON.parse(last_response.body)
    assert_equal json_response['message'], 'path not found'

    get '/six', format: 'json'
    assert_equal last_response.status, 404
    json_response = JSON.parse(last_response.body)
    assert_equal json_response['message'], 'path not found'
  end
end
