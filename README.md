Steps to Use:

* Clone the repository
   
   ```
     git clone git@bitbucket.org:akkee19/oneto1.git
   ```

* Install docker and ensure its running (steps out of scope)

* Build the app image locally

   ```
     cd oneto1 && docker build -t oneto1 . 
   ```

* Run the app server with default `collection.csv`

   ```
     docker run -it -p 9292:9292 oneto1:latest rackup config.ru --host 0.0.0.0
   ```

  or run the app server with a custom collection csv file of your choice

   ```
     docker run -it -p 9292:9292 -v full_path_of_custom_collection_csv:/home/app/collection.csv oneto1:latest rackup config.ru --host 0.0.0.0
   ```

   eg.

   ```
     docker run -it -p 9292:9292 -v /Users/akashsrivastava/Desktop/collect.csv:/home/app/collection.csv oneto1:latest rackup config.ru --host 0.0.0.0
   ```

* Spin up a browser and go to `localhost:9292/one` or try `curl -H "Content-type: application/json" localhost:9292/one` from a terminal of your choice (If you use the default csv) or any othe path you have specified in your csv if you are using a custom collection csv


Running tests:

```
  docker run -it oneto1:latest ruby /home/test/rack_app_test.rb
```


Find Sample Postman collection for default csv in `test/Testing_one_to_1.postman_collection.json`