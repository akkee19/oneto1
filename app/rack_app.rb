require 'rack'
require 'json'
require 'csv'
class RackApp # :nodoc:
  def self.app
    Rack::Builder.new do
      run RackApp.new
    end
  end

  def call(env)
    req = Rack::Request.new(env)
    message, resp_status = get_message_and_response_status(req)
    [resp_status, { 'Content-Type' => 'application/json' }, [message]]
  end

  private

  def get_message_and_response_status(req)
    if path_data.key?(req.path)
      key, value = path_data[req.path].values_at(0, 1)
      message = { key: key, value: value }.to_json
      resp_status = 200
    else
      message = { message: 'path not found' }.to_json
      resp_status = 404
    end
    [message, resp_status]
  end

  def path_data
    paths = []
    begin
      CSV.foreach(collection_path, headers: true) do |row|
        paths << ["/#{row['key']}", [row['key'], row['value'].to_i]]
      end
    rescue StandardError => e
      puts 'Error in loading the csv file'
      puts e.message
    end
    paths.to_h
  end

  def collection_path
    if File.exist?(File.join(ENV['ROOT_DIR'], 'app', 'collection.csv'))
      File.join(ENV['ROOT_DIR'], 'app', 'collection.csv')
    else
      File.join(ENV['ROOT_DIR'], 'app', 'default_collection.csv')
    end
  end
end
